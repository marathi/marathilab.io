(function (global) {

    var navbar_layout = "static/layouts/nav.html";
    var footer_layout = "static/layouts/footer.html";
    var testJSON = "resources/test.json";

    var navHTML = {
        id: "#navbar1",
        url: navbar_layout,
        jsonResptype: false,
    }

    var testJSONHTML = {
        id: "#test",
        url: testJSON,
        jsonResptype: true,
    }

    var footerHTML = {
        id: "#footer1",
        url: footer_layout,
        jsonResptype: false,
    }


    var basepage = [navHTML, testJSONHTML, footerHTML]

    // Convenience function for inserting innerHTML for 'select'
    var insertHtml = function (selector, html) {
        var targetElem = document.querySelector(selector);
        targetElem.innerHTML = html;
    };

    // Return substitute of '{{propName}}' with propValue in given 'string'
    var insertProperty = function (string, propName, propValue) {
        var propToReplace = "{{" + propName + "}}";
        string = string
            .replace(new RegExp(propToReplace, "g"), propValue);
        return string;
    };
    var buildHTML = function (id, html) {
        insertHtml(id, html);
        // console.log(id,html);
    }
    var testcallback = function (data, status) {
        console.log("Data: " + data + "\nStatus: " + status);
    }
    var testJqueryGet = function (data) {
        var test = $.get(url, testcallback, type);
    }

    var setIndexHtml = function (id, url, jsonResptype) {
        console.log("id, url, jsonResptype:", id, url, jsonResptype);
        $ajaxUtils.sendGetRequest(url, function (resp) {
            insertHtml(id, resp)
            console.log(resp, id);
        }, jsonResptype);

    }

    // On page load (before images or CSS)
    document.addEventListener("DOMContentLoaded", function (event) {
        // Explicitly setting the flag FALSE to get non JSON from server processed into an object literal
        // basepage.forEach(element => $.get("resources/test.json",testcallback,"json"));
        for (i = 0; i < basepage.length; i++) {
            setIndexHtml(basepage[i].id, basepage[i].url, basepage[i].jsonResptype);
        };
    });
    document.addEventListener('DOMContentLoaded', (event) => {
        console.log('DOM fully loaded and parsed');
    });




})(window);